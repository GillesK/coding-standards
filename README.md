*Fairly reasonable HTML, CSS and JS coding standards

# Table of Contents

  1. [HTML & CSS](#html-and-css)
  2. [Javascript](#javascript)
  3. [Git](#git)


# HTML and CSS

**news-article.twig**

```html
<div class="news-article news-article--featured">
  <h2 class="news-article__title">Featured News Title</h2>
  <p>Lorem ipsum</p>
</div>
```
Notes:
1. Use [BEM](https://getbem.com/)
2. Use kebab-case

---

**_news-article.scss**
```scss
.news-article {
  @include box();
  background: white;
  
  &--featured {
    background: blue;
  }
  
  @include breakpoint(phone down) {
  	background: black;
  }
}

.news-article__title {
  color: white;
}
```

Notes:
1. File and class share name
1. Add `@include`s at the start of the class
2. Add the `media query` last
3. Do not style IDs or elements
4. Do not nest

---

# JavaScript

***news-article.twig***
```html
<div class="news-article">
  <h2 class="news-article__title">News Title</h2>
  <p>Lorem ipsum</p>
  <button class="js-favorite">Favorite Button</button>
</div>
```
***favorite.js***
```js
const favoriteButton = document.querySelector('.js-favorite');

if (favoriteButton) {
	//...
}
```

Notes:
1. File and js-hook share name
2. Use camelCase
3. Use .js-hooks

# Git

- Development branch name: **develop**
- Production branch name: **master**

Notes:
1. Do not commit/push directly to master
2. Create type based branches `git checkout -b type/descriptive-name`
Available types are: **feature**, **release**, **bugfix**, **hotfix**
3. Keep a clean repo
4. Try to use [atomic commits](https://en.wikipedia.org/wiki/Atomic_commit#Atomic_commit_convention)
5. Prefix unfinished commits with `WIP`
